-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 27, 2020 at 03:48 PM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `clouddatabase`
--

-- --------------------------------------------------------

--
-- Table structure for table `keygenrator`
--

CREATE TABLE IF NOT EXISTS `keygenrator` (
  `kid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(55) NOT NULL,
  `specialization` varchar(15) NOT NULL,
  `account` varchar(11) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`kid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `keygenrator`
--


-- --------------------------------------------------------

--
-- Table structure for table `system`
--

CREATE TABLE IF NOT EXISTS `system` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(22) NOT NULL,
  `userpass` varchar(22) NOT NULL,
  `post` varchar(22) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `system`
--

INSERT INTO `system` (`id`, `username`, `userpass`, `post`) VALUES
(1, 'key', '1234', 'key Generatir'),
(2, 'sanitizer', '1234', 'Sanitizer'),
(3, 'tpa', '1234', 'TPA'),
(4, 'cloud', '1234', 'Cloud');

-- --------------------------------------------------------

--
-- Table structure for table `userdetails`
--

CREATE TABLE IF NOT EXISTS `userdetails` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(22) NOT NULL,
  `userpass` varchar(22) NOT NULL,
  `useremail` varchar(22) NOT NULL,
  `usermobile` int(12) NOT NULL,
  `usercity` varchar(11) NOT NULL,
  `userkey` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `account` varchar(11) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `userdetails`
--

INSERT INTO `userdetails` (`uid`, `username`, `userpass`, `useremail`, `usermobile`, `usercity`, `userkey`, `date`, `account`) VALUES
(1, 'abhi', 'abhi', 'abhi@fmail.com', 2147483647, 'Kopargaon', 'jk33wkevqyMdEiy7WLGJ1kf7eoanFAUf4mT20Htzpbk=', '2020-04-17', 'Active'),
(2, 'Sagar', 'sagar', 'sagar@gmail.com', 2147483647, 'Shirdi', '6dr6MDefn9vNeDP2twtMhOH9uNLpB8RLM1rU6KONDWg=', '2020-04-18', 'Active'),
(3, 'pooja', '1234', 'pooja@gmail.com', 1234567890, 'Kopargaon', 'ylnYRAKTS1tEQDPxwM8WS5HzHPkCAcHHxfSda1yUHN4=', '2020-04-19', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `userfiles`
--

CREATE TABLE IF NOT EXISTS `userfiles` (
  `fid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `username` varchar(22) NOT NULL,
  `useremail` varchar(22) NOT NULL,
  `usermobile` int(22) NOT NULL,
  `usercity` varchar(11) NOT NULL,
  `userfile` text NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`fid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `userfiles`
--

INSERT INTO `userfiles` (`fid`, `uid`, `username`, `useremail`, `usermobile`, `usercity`, `userfile`, `date`) VALUES
(4, 2, 'Sagar', 'sagar@gmail.com', 2147483647, 'Shirdi', 'encypt.txt.encrypted', '2020-04-22'),
(5, 2, 'Sagar', 'sagar@gmail.com', 2147483647, 'Shirdi', 'encypt.txt.encrypted', '2020-04-22'),
(6, 2, 'Sagar', 'sagar@gmail.com', 2147483647, 'Shirdi', 'encypt.txt.encrypted', '2020-04-22'),
(7, 3, 'pooja', 'pooja@gmail.com', 1234567890, 'Kopargaon', 'encypt.txt.encrypted', '2020-04-23');

-- --------------------------------------------------------

--
-- Table structure for table `userrecord`
--

CREATE TABLE IF NOT EXISTS `userrecord` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `username` varchar(22) NOT NULL,
  `filekey` varchar(64) NOT NULL,
  `filename` varchar(100) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `userrecord`
--

INSERT INTO `userrecord` (`id`, `uid`, `username`, `filekey`, `filename`, `date`) VALUES
(1, 2, 'Sagar', '6dr6MDefn9vNeDP2twtMhOH9uNLpB8RLM1rU6KONDWg=', 'test.txt', '2020-04-22'),
(2, 2, 'Sagar', '6dr6MDefn9vNeDP2twtMhOH9uNLpB8RLM1rU6KONDWg=', 'test.txt', '2020-04-22'),
(3, 3, 'pooja', 'ylnYRAKTS1tEQDPxwM8WS5HzHPkCAcHHxfSda1yUHN4=', 'test.txt', '2020-04-23');

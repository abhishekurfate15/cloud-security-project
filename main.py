
from flask import Flask, render_template, request
from flask_sqlalchemy import SQLAlchemy
import json
from datetime import date
from cryptography.fernet import Fernet 


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "mysql://root:@localhost/clouddatabase"
db = SQLAlchemy(app)




class Userdetails(db.Model):
    uid = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), nullable=False)
    userpass = db.Column(db.String(50),nullable=False)
    useremail = db.Column(db.String(200), nullable=False)
    usermobile = db.Column(db.String(100), nullable=False)
    usercity = db.Column(db.String(50), nullable=False)
    account = db.Column(db.String(50), nullable=False)
    userkey = db.Column(db.String(50), nullable=False)
    date = db.Column(db.String(50), nullable=False)
    


class Userfiles(db.Model):
    fid = db.Column(db.Integer, primary_key=True)
    uid = db.Column(db.Integer, nullable=False)
    username = db.Column(db.String(50), nullable=False)
    useremail = db.Column(db.String(50),nullable=False)
    usermobile = db.Column(db.String(200), nullable=False)
    usercity = db.Column(db.String(200), nullable=False)
    userfile = db.Column(db.String(200), nullable=False)
    date = db.Column(db.String(200), nullable=False)


class System(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), nullable=False)
    userpass = db.Column(db.String(50),nullable=False)
    post = db.Column(db.String(200), nullable=False)

class Userrecord(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    uid = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), nullable=False)
    filekey = db.Column(db.String(50),nullable=False)
    filename = db.Column(db.String(200), nullable=False)
    date = db.Column(db.String(200), nullable=False)


@app.route("/")
def home():
    return render_template('index.html')


@app.route("/userhome")
def userhome():

    return render_template('userhome.html')

@app.route("/userpg")
def userpage():

    return render_template('userpg.html')

    
@app.route("/addnewuser", methods= ['GET', 'POST'])

def adduser():
    if(request.method=='POST'):
        uname = request.form.get('uname')
        upass = request.form.get('upass')
        uemail = request.form.get('uemail')
        umobile = request.form.get('umobile')
        ucity = request.form.get('ucity')
        today = date.today()

        entry = Userdetails(username = uname, userpass = upass, useremail = uemail, usermobile = umobile, usercity = ucity,account='inactive',userkey = '',date= today )
        db.session.add(entry)
        db.session.commit()
    return render_template('userlogin.html')
    
@app.route("/userlogin")
def userlogin():

    return render_template('userlogin.html')

@app.route("/userloginvalid", methods= ['GET', 'POST'])
def showdata():

 
    if(request.method=='POST'):
            
        uemail = request.form.get('uemail')
        upass = request.form.get('upass')
        logincheck = Userdetails.query.filter_by(useremail=uemail,userpass=upass).first_or_404(description='Please Enter Correct Credentials{}'.format(uemail))
        activitedlogin = Userdetails.query.filter_by(useremail=uemail,account = 'active').first_or_404(description='Account not Activated {}'.format(uemail))
                
            
    
        return render_template('userhome.html', activitedlogin = activitedlogin)  
    else:
        return render_template('userlogin.html')

       
     
        

@app.route("/userfile/<string:id>", methods= ['GET', 'POST'])
def userfile(id):
    username = request.form.get('username')
    userfile = request.form.get('userfile')
    sen = Userdetails.query.filter_by(uid=id).first()
    today = date.today()
    key = sen.userkey
    
    with open(userfile, 'rb') as f:
        data = f.read()

        fernet = Fernet(key)
        encrypted = fernet.encrypt(data)
    with open('encypt.txt.encrypted', 'wb') as f:
        f.write(encrypted)
    userrr = Userrecord(uid = id, username = sen.username, filekey = sen.userkey, filename = userfile, date = today)
    db.session.add(userrr)
    db.session.commit()
    addfile = Userfiles(uid = id, username = sen.username, useremail = sen.useremail,usermobile = sen.usermobile,usercity = sen.usercity ,userfile = "encypt.txt.encrypted", date = today )
    db.session.add(addfile)
    db.session.commit()
    return render_template('userlogin.html')

@app.route("/userviewuploadedfile/<string:id>")
def userviewuploadedfile(id):
    viewup = Userrecord.query.filter_by(uid=id).all()
    passvalue = Userrecord.query.filter_by(uid=id).first()
    return render_template('userviewuploadedfile.html',viewup=viewup, passvalue=passvalue)



@app.route("/userauditingrequest/<string:id>")
def userauditingrequest(id):
    viewup = Userrecord.query.filter_by(uid=id).all()
    print(id)
    return render_template('userauditingrequest.html',viewup=viewup)

@app.route("/keygenerator")
def keygenerator():

    return render_template('keygenratorlogin.html')

@app.route("/keygeneratorlogin", methods= ['GET', 'POST'])
def key():
    if(request.method=='POST'):
    
            uemail = request.form.get('uemail')
            upass = request.form.get('upass')
            myUser = System.query.filter_by(username=uemail,userpass = upass).first_or_404(description='Please Enter Valid Email Id and Password {}'.format(uemail))
            return render_template('keygeneratorhome.html') 

@app.route("/keygeneratoractivateuser")
def keygeneratoractive():
    gene = Userdetails.query.filter_by(account='active').all()
    return render_template('keygeneratoractivateuser.html',gene=gene)

@app.route("/keygeneratorpendingactivation")
def keygeneratorpending():
    pending = Userdetails.query.filter_by(account='inactive').all()
    return render_template('keygeneratorpendingactivation.html',pending=pending)

@app.route("/privatekeygenerator/<string:id>")
def activatekeygene(id):
    
    this = Userdetails.query.filter_by(uid=id).first()
    key = Fernet.generate_key()
    this.account = "Active"
    this.userkey = key
    db.session.commit()
    pending = Userdetails.query.filter_by(account='inactive').all()
    return render_template('keygeneratorpendingactivation.html',pending=pending)

@app.route("/sanitizer")
def sanitizer():

    return render_template('sanitizerlogin.html')

@app.route("/sanitizerlogin", methods= ['GET', 'POST'])
def sanitizerlogin():
    if(request.method=='POST'):
    
            username = request.form.get('username')
            userpass = request.form.get('userpass')
            myUser = System.query.filter_by(username=username,userpass = userpass).first_or_404(description='Please Enter Valid Email Id and Password {}'.format(username))
            alldata = Userfiles.query.all()
            return render_template('sanitizerhome.html', alldata=alldata) 

@app.route("/tpa")
def tpa():

    return render_template('tpalogin.html')

@app.route("/tpalogin", methods= ['GET', 'POST'])
def tpalogin():
    if(request.method=='POST'):
    
            username = request.form.get('username')
            userpass = request.form.get('userpass')
            myUser = System.query.filter_by(username=username,userpass = userpass).first_or_404(description='Please Enter Valid Email Id and Password {}'.format(username))
            return render_template('tpahome.html') 

@app.route("/cloud")
def cloud():

    return render_template('cloudlogin.html')

@app.route("/cloudlogin", methods= ['GET', 'POST'])
def cloudlogin():
    if(request.method=='POST'):
    
            username = request.form.get('username')
            userpass = request.form.get('userpass')
            myUser = System.query.filter_by(username=username,userpass = userpass).first_or_404(description='Please Enter Valid Email Id and Password {}'.format(username))
            return render_template('cloudhome.html') 



app.run(debug=True)




